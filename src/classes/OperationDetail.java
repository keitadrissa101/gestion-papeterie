/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author bureau
 */
public class OperationDetail {
    private int idp;
    private int ido;
    private String categorie;
    private String designation;
    private int pu;
    private int qte;
    private int be;
    private Date dated;
    private int num;
    private int uid;
    private String login;

    public OperationDetail() {
    }
    
    public OperationDetail( String categorie, String designation, int pu, int qte, int be, int uid, String login) {
        this.categorie = categorie;
        this.designation = designation;
        this.pu = pu;
        this.qte = qte;
        this.be = be;
        this.uid = uid;
        this.login = login;
    }

    public OperationDetail(int ido, String categorie, String designation, int pu, int qte, int be, Date dated, int num, int uid, String login) {
        this.ido = ido;
        this.categorie = categorie;
        this.designation = designation;
        this.pu = pu;
        this.qte = qte;
        this.be = be;
        this.dated = dated;
        this.num = num;
        this.uid = uid;
        this.login = login;
    }

    public OperationDetail(int idp, int ido, String categorie, String designation, int pu, int qte, int be, Date dated, int num, int uid, String login) {
        this.idp = idp;
        this.ido = ido;
        this.categorie = categorie;
        this.designation = designation;
        this.pu = pu;
        this.qte = qte;
        this.be = be;
        this.dated = dated;
        this.num = num;
        this.uid = uid;
        this.login = login;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getPu() {
        return pu;
    }

    public void setPu(int pu) {
        this.pu = pu;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getBe() {
        return be;
    }

    public void setBe(int be) {
        this.be = be;
    }

    
}
